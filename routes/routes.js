const express = require('express');
const router = express.Router();


router.get('/', (req, res) => {
    res.send('Hello from router');
});

router.get('/events/list', (req, res) => {
    res.send('Future route for events list');
});

module.exports = router;