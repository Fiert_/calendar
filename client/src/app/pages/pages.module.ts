import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PagesRoutingModule } from "./pages.routing.module";
import { CalendarComponent } from "./calendar/calendar.component";
import { LoginComponent } from "./login/login.component";
import { EventComponent} from './calendar/event/event.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CalendarComponent,
    LoginComponent,
    EventComponent
  ],
  imports: [
    ReactiveFormsModule,
    PagesRoutingModule,
    CommonModule
  ],
  providers: []
})

export class PagesModule {
}
