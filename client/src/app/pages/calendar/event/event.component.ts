import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-event',
  styleUrls: ['./event.component.css'],
  template: `
    <div class="row">
      <div class="event-box"
           [style.height.px]="eventHeight"
           [style.width.px]="eventWidth">
        <h6 class="event-text">{{text}} </h6>
      </div>
    </div>
  `,
})

export class EventComponent {

  @Input()
  public eventHeight: number;

  @Input()
  public eventWidth: number;

  @Input()
  public text: string;

  constructor() {
  }
}
