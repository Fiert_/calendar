import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css'],
})

export class CalendarComponent implements OnInit {

  public daily = [];

  public formGroup : FormGroup

  private dateFrom = moment().year(moment().year()).date(moment().date()).hour(8).startOf('hour');
  private dateTo = moment().year(moment().year()).date(moment().date()).hour(17).startOf('hour');

  constructor() {
    this.formGroup = new FormGroup({
      start: new FormControl('',[
        Validators.required,
      ]),
      duration: new FormControl('', [
        Validators.required,
      ]),
      title: new FormControl('', [
        Validators.required,
      ])
    });

    let counter = 0;
    let from = this.dateFrom;
    while (from <= this.dateTo) {
      this.daily.push({
        date: from.format('H:mm'),
        value: counter++,
        eventCount: 0,
        events: [],
      });
      from = from.add(1, 'm');
    }
  }

  ngOnInit() {
    console.log(this.daily);
  }
  onSubmit() {
    let currentValue =
    console.log(typeof this.formGroup.get('start').value);
    this.daily.forEach((elem) => {
      if (elem.date === this.formGroup.get('start').value){
        if (elem.eventCount === 0) {
          elem.events.push({
            width: 200,
            height: this.formGroup.get('duration').value * 2,
            title: this.formGroup.get('title').value,
          });
        }
        else if (elem.eventCount === 1) {
          elem.events[0].width = 100;
          elem.events.push({
            width: 100,
            height: this.formGroup.get('duration').value * 2,
            title: this.formGroup.get('title').value,
          });
        }

      }
      while (currentValue < elem.value + this.formGroup.get('duration').value) {
        elem.eventCount++;
        currentValue = currentValue +=1;
        console.log(currentValue);
      }
    });
    console.log(this.daily);
  }
}
