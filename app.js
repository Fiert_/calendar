const express = require('express');

const app = express();
const router = require('./routes/routes');

const port = 3001;


app.listen(port, () => {
  console.log(`Server listening on port:${port}`);
});

app.use('/api', router);

app.listen();
